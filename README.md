# Aiguilleur.ca in Dockers

## Dependencies

* Git Bash
* <a target="_blank" href="https://docs.docker.com/get-docker/">docker</a>
* <a target="_blank" href="https://docs.docker.com/compose/install/">docker-compose</a>

## Select config file

1. Select a new config file

    ```bash
    $ sh select_config_file.sh config-dev.env
    ```

1. Check selected config file

    ```bash
    $ sh check_config_file.sh
    ```

## Run `docker-compose` through `compose.sh`

1. Building dockers

    ```bash
    sh compose.sh build
    ```

1. Building dockers w/o cache

    ```bash
    sh compose.sh build --no-cache
    ```

1. Building one particular docker

    ```bash
    sh compose.sh build nginx
    ```

1. Running the dockers

    ```bash
    sh compose.sh up -d
    ```

## XXX: docker volumes are mounted only at run time

* When building a docker, the volume is not mounted

* The volume gets mounted right before running the docker

* Commands manipulating files on the volume do not have the same effect at build time Vs run time

## Other scripts

```
bash_on_fresh_docker.sh                  create a new docker from build, and run a bash on it
                                         (this docker is not part of the network specified in docker-compose.yml)

bash_on_running_docker.sh                run bash on the some running docker
                                         (this is the docker connected to the network specifid in docker-compose.yml)

backup_mongodb.sh                        extract data from mongodb docker to localhost
restore_mongodb.sh                       push data from localhsot to mongodb docker

initialize_letsencrypt.sh                run certbot a first time to create initial files in letsencrypt volume (mounted on /etc/letsencrypt)

rebuild_aquiletour.sh                    build .jar and .js on docker, then copy to localhost 

redeploy.sh                              rebuild and restart Dockers 

regenerate_dh2048.sh                     create a dh2048.pem on docker, the copy to localhost
regenerate_dummy_ssl_certificate.sh      create dummy ssl certificate on docker, then copy to localhost
regenerate_opendkim_key.sh               create opendkim key on docker, then copy to localhost

remove_letsencrypt_volume.sh             clear letsencrypt volume
remove_mongodb_volumes.sh                clear mongodb volumes
```
