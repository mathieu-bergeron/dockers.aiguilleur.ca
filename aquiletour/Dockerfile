ARG BUILD_VERSION

################################
### INIT (BUILD, DEV & PROD) ###
################################
FROM almalinux:8 as init

RUN dnf install -y epel-release
RUN dnf update -y
RUN dnf install -y java-11-openjdk-headless
RUN dnf install -y inotify-tools

RUN cp /usr/share/zoneinfo/America/Montreal /etc/localtime
RUN echo "America/Montreal" >  /etc/timezone

RUN dnf install -y glibc-langpack-fr
ENV LC_ADDRESS fr_CA.UTF-8
ENV LC_NAME fr_CA.UTF-8
ENV LC_MONETARY fr_CA.UTF-8
ENV LC_PAPER fr_CA.UTF-8
ENV LANG fr_CA.UTF-8
ENV LC_IDENTIFICATION fr_CA.UTF-8
ENV LC_TELEPHONE fr_CA.UTF-8
ENV LC_MEASUREMENT fr_CA.UTF-8
ENV LC_TIME fr_CA.UTF-8
ENV LC_NUMERIC fr_CA.UTF-8
ENV LC_ALL ""

RUN mkdir /app
WORKDIR /app


################
### DEV ONLY ### 
################
FROM init as dev

RUN dnf install -y vim 
RUN dnf install -y curl 
RUN dnf install -y nmap-ncat 
RUN dnf install -y nmap 
RUN dnf install -y net-tools 
RUN dnf install -y iputils 
RUN dnf install -y bind-utils

RUN mkdir -p /etc/letsencrypt
ARG CONFIG_DIR
COPY ${CONFIG_DIR}/ssl /etc/letsencrypt/live

ARG THIS_DOCKER_DIR
COPY ${THIS_DOCKER_DIR}/up-dev.sh /app/up.sh

##################
###  PROD ONLY ### 
##################
FROM init as prod

ARG THIS_DOCKER_DIR
COPY ${THIS_DOCKER_DIR}/up-prod.sh /app/up.sh
COPY ${THIS_DOCKER_DIR}/watchdog.sh /app/watchdog.sh
RUN chmod u+x /app/watchdog.sh

########################
### RUN (DEV & PROD) ###
########################
FROM ${BUILD_VERSION}

ARG BUILD_VERSION
ARG THIS_DOCKER_DIR
ARG CONFIG_DIR
ARG CONFIG_FILE
ARG BUILD_DIR
ARG SRC_DIR

COPY ${THIS_DOCKER_DIR}/init.sh /app/init.sh
COPY ${THIS_DOCKER_DIR}/start_service.sh /app/start_service.sh
COPY ${THIS_DOCKER_DIR}/stop_service.sh /app/stop_service.sh
COPY ${THIS_DOCKER_DIR}/restart_service.sh /app/restart_service.sh
COPY ${THIS_DOCKER_DIR}/on_restart_needed.sh /app/on_restart_needed.sh
COPY ${THIS_DOCKER_DIR}/queue_service_restart.sh /app/queue_service_restart.sh
RUN chmod u+x /app/init.sh
RUN chmod u+x /app/start_service.sh
RUN chmod u+x /app/stop_service.sh
RUN chmod u+x /app/restart_service.sh
RUN chmod u+x /app/on_restart_needed.sh
RUN chmod u+x /app/queue_service_restart.sh
RUN chmod u+x /app/up.sh

RUN mkdir /root/.aiguilleur
COPY ${CONFIG_DIR}/${CONFIG_FILE} /root/.aiguilleur/config.json

COPY ${BUILD_DIR}/aquiletour_server-all.jar ./aquiletour_server-all.jar

RUN mkdir -p src/main/resources
COPY ${SRC_DIR}/aquiletour_server/src/main/resources src/main/resources
COPY ${BUILD_DIR}/aquiletour_jsweet-${BUILD_VERSION} src/main/resources/public/js/aquiletour_jsweet

CMD ./up.sh
