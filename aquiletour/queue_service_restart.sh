#!/usr/bin/sh

echo "[$(date)] Queueing Aquiletour restart in 5 seconds"
sleep 5;

./restart_service.sh
rm .restart_lock
