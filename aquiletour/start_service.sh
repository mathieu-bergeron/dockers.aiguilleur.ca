#!/usr/bin/sh

if [ -e .service_pid ]; then
    service_pid=$(cat .service_pid)
fi

if [ "$service_pid" != "" ]; then
    echo "[$(date)] Aquiletour process already running"
    exit 1
fi

echo "[$(date)] Starting Aquiletour process"

java -cp aquiletour_server-all.jar ca.aquiletour.server.vertx.JavaMainVertx 2>> exceptions.log &

service_pid=$!
echo $service_pid > .service_pid

echo "[$(date)] Aquiletour process started with pid $service_pid"
