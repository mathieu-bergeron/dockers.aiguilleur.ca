#!/usr/bin/sh

if [ -e .service_pid ]; then
    service_pid=$(cat .service_pid)
fi

if [ "$service_pid" = "" ]; then
    echo "[$(date)] Aquiletour process not running"
    exit 1
fi

echo "[$(date)] Killing Aquiletour process with pid $service_pid"

kill $service_pid
rm .service_pid
