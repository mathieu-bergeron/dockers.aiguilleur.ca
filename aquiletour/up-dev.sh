#!/usr/bin/sh

./init.sh

touch email_codes.txt
tail -f email_codes.txt &

touch exceptions.log
tail -f exceptions.log &

./start_service.sh

inotifywait -m -e CREATE /etc/letsencrypt/live/aiguilleur.ca | while read event;
do
    echo "[$(date)] SSL certificate changed with event: $event";
    ./on_restart_needed.sh
done



