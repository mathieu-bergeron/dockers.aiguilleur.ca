#!/usr/bin/sh

./init.sh

./watchdog.sh &

./start_service.sh

inotifywait -m -e CREATE /etc/letsencrypt/live/aiguilleur.ca | while read event;
do
    echo "[$(date)] SSL certificate changed with event: $event";
    ./on_restart_needed.sh
done
