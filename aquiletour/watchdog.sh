#!/usr/bin/sh

while true; 
do
    sleep 5;

    if [ -e .service_pid ]; then
        service_pid=$(cat .service_pid)
    fi

    if [ "$service_pid" != "" ]; then

        if [ ! -e /proc/$service_pid ]; then

            rm .service_pid

            cat exceptions.log
            echo "[$(date)] Aquiletour process crashed... restarting" 
            echo "[$(date)] Aquiletour process crashed... restarting" >> crashes.log

            ./start_service.sh
        fi
    fi
done

