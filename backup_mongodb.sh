user_name=$(whoami)
container_name=mongodb

echo === XXX: removing previous backup ===
sleep 0.2

rm -rvf backup_mongodb/*

sh compose.sh exec mongodb mongodump

sudo docker cp $container_name:/dump/admin backup_mongodb/
sudo docker cp $container_name:/dump/aquiletour backup_mongodb/

sudo chown -R $user_name:$user_name backup_mongodb
