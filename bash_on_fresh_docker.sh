
docker_name=$1

if [ "$docker_name" = "" ]; then
    echo usage $0 docker_name
    exit 0
fi

container_name=bash_on_$docker_name

sh compose.sh run --entrypoint bash --name $container_name $docker_name

sudo docker rm $container_name
