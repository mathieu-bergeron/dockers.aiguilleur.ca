
docker_name=$1

if [ "$docker_name" = "" ]; then
    echo usage $0 docker_name
    exit 0
fi

sh compose.sh exec $docker_name bash
