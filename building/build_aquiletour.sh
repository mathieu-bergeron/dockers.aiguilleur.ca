#!/usr/bin/sh
cd /src
sh gradlew js
sh gradlew aquiletour_server:shadow

cp -rvf aquiletour_server/src/main/resources/public/js/aquiletour_jsweet aquiletour_jsweet-prod

sh scripts/patch_js_for_dev.sh

cp -rvf aquiletour_server/src/main/resources/public/js/aquiletour_jsweet aquiletour_jsweet-dev
