#!/usr/bin/sh
cd /aiguilleur.ca

openssl req -new -x509 -nodes -subj "/CN=localhost" -newkey rsa:2048 -keyout ca.key -out ca.crt
openssl req -new -nodes -sha256 -newkey rsa:2048 -keyout domain.key -config dummy_ssl_req.conf -out domain.csr
openssl x509 -req -in domain.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out domain.crt -days 500 -sha256 -extfile dummy_ssl_req.conf 

cp domain.key privkey.pem 
cat domain.crt ca.crt > fullchain.pem

