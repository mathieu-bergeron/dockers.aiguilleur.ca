if [ -e .current_config_file ]; then
    config_file=$(readlink .current_config_file) 
fi

if [ "$config_file" = "" ]; then 
    echo ""
    echo "NO CONFIG FILE"
    echo "Please select a config file with:"
    echo ""
    echo "    $ sh select_config_file.sh FILE"
    echo ""
    exit 1

else

    echo "CONFIG FILE: $config_file"
    exit 0

fi
