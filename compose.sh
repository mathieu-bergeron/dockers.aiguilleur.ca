echo ""

sh check_config_file.sh || exit 


echo "ARGS: $@"
echo ""
sleep 0.2

config_file=$(readlink .current_config_file) 

sudo docker-compose --env-file $config_file $@
