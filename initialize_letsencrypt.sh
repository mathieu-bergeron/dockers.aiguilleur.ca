user_name=$(whoami)
container_name=initilize_letsencrypt

sh compose.sh down

sh remove_letsencrypt_volume.sh

sh compose.sh build nginx

#sh compose.sh run --entrypoint "/initialize_letsencrypt.sh" --service-ports --name $container_name nginx
sh compose.sh run --entrypoint bash --service-ports --name $container_name nginx

sudo docker rm $container_name
