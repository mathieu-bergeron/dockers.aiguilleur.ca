#!/usr/bin/sh

if [ -e .restart_lock ]; then
    echo "[$(date)] Restart is already queued... exiting"
    exit 0
fi

touch .restart_lock
./queue_service_restart.sh &
