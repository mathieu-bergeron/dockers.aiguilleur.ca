#!/usr/bin/sh

echo "[$(date)] Queueing postfix restart in 5 seconds"
sleep 5;

postfix stop
postfix start

rm .restart_lock
