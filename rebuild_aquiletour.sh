user_name=$(whoami)
container_name=rebuild_aquiletour

sh compose.sh build building
sh compose.sh run --entrypoint "/build_aquiletour.sh" --name $container_name building

sudo docker cp $container_name:/src/aquiletour_server/build/libs/aquiletour_server-all.jar build/
sudo docker cp $container_name:/src/aquiletour_jsweet-prod build/
sudo docker cp $container_name:/src/aquiletour_jsweet-dev build/

sudo chown -R $user_name:$user_name build/

sudo docker rm $container_name
