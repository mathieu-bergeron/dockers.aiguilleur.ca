user_name=$(whoami)
container_name=generate_dh2048

sh compose.sh build building
sh compose.sh run --entrypoint "/generate_dh2048.sh" --name $container_name building

sudo docker cp $container_name:/dh2048.pem config/dh2048.pem

sudo chown $user_name:$user_name config/dh2048.pem

sudo docker rm $container_name
