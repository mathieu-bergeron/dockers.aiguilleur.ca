user_name=$(whoami)
container_name=generate_opendkim_key


sh remove_letsencrypt_volume.sh

sh compose.sh build building
sh compose.sh run --entrypoint "/generate_dummy_ssl_certificate.sh" --name $container_name building

rm -rf config/ssl/aiguilleur.ca
mkdir -p config/ssl/aiguilleur.ca
sudo docker cp $container_name:/aiguilleur.ca/privkey.pem config/ssl/aiguilleur.ca/
sudo docker cp $container_name:/aiguilleur.ca/fullchain.pem config/ssl/aiguilleur.ca/

sudo chown -R $user_name:$user_name config/ssl/aiguilleur.ca

sudo docker rm $container_name
