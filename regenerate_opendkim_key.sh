echo "===="
echo "===="
echo "==== WARNING: must then change DNS entry to fit new key ==="
echo "===="
echo "===="
sleep 3


user_name=$(whoami)
container_name=generate_opendkim_key

sh compose.sh build building
sh compose.sh run --entrypoint "/generate_opendkim_key.sh" --name $container_name building

rm -rf config/opendkim
sudo docker cp $container_name:/opendkim config/

sudo chown -R $user_name:$user_name config/opendkim

sudo docker rm $container_name
