user_name=$(whoami)
container_name=mongodb

sh compose.sh down

sh remove_mongodb_volumes.sh

sh compose.sh build mongodb

sh compose.sh up -d mongodb

sudo docker cp backup_mongodb $container_name:/dump

sh compose.sh exec mongodb "mongorestore /dump"

sh compose.sh down

