config_file=$1

if [ -e .current_config_file ]; then
    previous_config_file=$(readlink .current_config_file) 
fi

if [ -e .current_config_file ]; then
    rm .current_config_file
fi

if [ "$config_file" != "" -a -e "$config_file" ]; then

    echo "SELECTING $config_file"
    ln -s $config_file .current_config_file

else
    echo "FILE NOT FOUND"
    echo "USING DEFAULT config-dev.env"
    config_file="config-dev.env"
    ln -s config-dev.env .current_config_file
fi

if [ "$previous_config_file" != "" -a "$previous_config_file" != "$config_file" ]; then
    echo "CONFIG FILE CHANGED... rebuilding"
    sleep 0.2
    sh compose.sh build
fi
